# Graphab - QGIS plugin

## Definition

Graphab is a geographic software that modelizes green corridors and connexions thanks to the Graph Theory.

It has a standalone version, but is also integrated as a plugin in QGis.

## Download plugin in Qgis

You need [JAVA 8](https://adoptium.net/?variant=openjdk8) or more installed on your computer.

    Plugins > Manage and install Plugins > Search for Graphab

A new tool bar is popping up with buttons.

## Get input data

You can use [BD Topo, BD Ortho or other datas from IGN geoservices](https://geoservices.ign.fr/telechargement).

For this example, we'll use [BD TOPO® Départementale Shapefile - D001 Ain - Septembre 2021](https://geoservices.ign.fr/ressource/162022).

    // The ZIP file used in this example

    BDTOPO_3-0_TOUSTHEMES_SHP_LAMB93_D001_2021-09-15.7z

You need to unzip it.

## Configure input data

Hit the `french map` button in QGis tool bar

### > Layers tab

---

1/ Hit the `+` button over the vector field, on the right.

2/ Load a vector file.

    // The one we're using in this example

    BDTOPO_3-0_TOUSTHEMES_SHP_LAMB93_D001_2021-09-15 >
    BDTOPO >
    1_DONNEES_LIVRAISON_2021-09-00165 >
    BDT_3-0_SHP_LAMB93_D001-ED2021-09-15 >
    OCCUPATION_DU_SOL >
    ZONE_DE_VEGETATION.shp

3/ Choose the value used to rasterize the vector layer, it can be a field present in the layer or a unique value.

    // We'll select the nature field that will be used for value.

    Field > Nature

4/ Repeat process to add new layers (optional)

    // For example, add:

    BDTOPO_3-0_TOUSTHEMES_SHP_LAMB93_D001_2021-09-15 >
    BDTOPO >
    1_DONNEES_LIVRAISON_2021-09-00165 >
    BDT_3-0_SHP_LAMB93_D001-ED2021-09-15 >
    ZONES_REGLMENTEES >
    PARC_OU_RESERVE.shp

5/ Order layers (optional). The higher gives priority to all their encodings in general.

### > Merge tab

---

In this tab, you need to order more precisely the different codes of all the layers. The highest in the order, the more weight it has. The output map is a raster.

> The code represents the land occupation. It will allows to define the nodes.
> The value of resistance represents the difficulty of crossing this land. It will allow to calculate the links between the nodes.

The highest layer gives the final value of the pixel.

### > Output tab

---

You can configure the landscape map you'll be creating.

You can change the resolution.

You can choose the out file name and path.

Hit `start` to execute the map creation.

### > Log tab

---

Will give you process information and errors. If no issue, the map is now loading into Qgis. You can close the modal.

### > Back to main map

---

You can click on the merged layer on the side and export as Geotiff or Mbtiles.

You can export the configuration with the `disk` button.

You can select an existing configuration with the `folder` button.

## Overall approach and definition

There are different steps to create a landscape graph with Graphab for Qgis. You will need to create a Graphab project

### 1/ Creating the land cover map

---

This map gathers multiple landscape categories ordered depending on their qualities. You need scientific litterature and research to be accurate.

Each category is associated with a resistance value, representing the difficulty / permeability it ahs.

> A node / patch is the combination of all close-by cells belonging to the same category. Each node has a capacity / demographic potential. Usually it is related to its size, as a bigger size has more resources to offer.

### 2/ Creating the linkset

---

> A link is the connexion / potential between two patches. It is created if a species can cross this in-between land. Two choices need to be made: the topology and the ponderation.

**Links topology**

> Possibility 1: Complete topology. All links between patches are taken into account. They can be over each other.

> Possibility 2: Partial / Plane topology. It's an approximation and a simplification of the links. It doesn't allow superimposition.

**Links ponderation**

The action of attributing a value and cost for a species to trvael between the two patches. It is directly linked to the difficulty / resistance values given to the geographic categories.

The final graph should only represents valid links which weight / cost is inferior to a certain limit.

**Distance between two patches**

> Euclidian: meters/distance as the crow flies. Considers the map as a uniform entity.

> Lower cost distance. Depends on the values/ resistance of the land categories.

### 3/ Creating a graph

---

The same map and project can be used to create multiple graphs. Each graph is based on a linkset.

**Pruning the linkset**

Cleaning up the links depending on distance.

### 4/ Creating corridors

---

Space between two patches that might be crossed by species.
