# Green Corridor

## Sources

- [BioDispersal: une extension QGIS pour cartographier les continuités écologiques](https://naturagis.fr/qgis/biodispersal-extension-qgis-continuites-ecologiques-tvb/)

- [GitHub BioDispersal](https://github.com/MathieuChailloux/BioDispersal)

- [User guide Biodispersal](https://github.com/MathieuChailloux/BioDispersal/blob/master/docs/en/BioDispersalUserGuide_v1.1.pdf)

- [Data OpenStreetMap GeoJSON France exemple](https://www.data.gouv.fr/fr/datasets/decoupage-administratif-communal-francais-issu-d-openstreetmap/)

- [données mobilisables trame verte et bleue](http://www.trameverteetbleue.fr/outils-methodes/donnees-mobilisables)

- [Méthode de perméabilité des milieux: Concepts et principe](https://www.umr-tetis.fr/jdownloads/plateformes/MethodePermeabiliteMilieux.pdf)

- [Graphab 14 RÉALISATIONS À DÉCOUVRIR](https://sourcesup.renater.fr/www/graphab/download/Livret_Graphab%20-%2014%20realisations%20a%20decouvrir_2018.pdf)

- [Manuel Graphab](https://sourcesup.renater.fr/www/graphab/download/manual-2.0-fr.pdf)

- [Graphab for QGIS - User Guide](https://sourcesup.renater.fr/www/graphab/download/graphab4qgis_manuel-en.pdf)

- [Graphab gitlab](https://gitlab-mshe.univ-fcomte.fr/thema/graphab4qgis/-/tree/master)

- [Fiches méthodo Graphab](https://www.researchgate.net/publication/342344729_Fiches_Graphab_Tutoriel)

- [IGN BD TOPO](https://geoservices.ign.fr/bdtopo)

- [IGN DB ORTHO](https://geoservices.ign.fr/bdortho)
